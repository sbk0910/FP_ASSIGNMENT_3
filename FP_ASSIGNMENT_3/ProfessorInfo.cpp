#include "ProfessorInfo.h"
#include "StudentInfo.h"

ProfessorInfo::ProfessorInfo(const ProfessorInfo& _prof) {
	strcpy(mName, _prof.mName);
	mProfID = _prof.mProfID;
	mSalary = _prof.mSalary;
}

ProfessorInfo::ProfessorInfo(string sData) {
	stringstream ss;
	ss.str(sData);

	string split_by_comma[3];
	SplitByComma(sData, split_by_comma);

	strcpy(mName, split_by_comma[0].c_str());
	mProfID = atoi(split_by_comma[1].c_str());
	mSalary = atoi(split_by_comma[2].c_str());
	//cout << mName << " " << mProfID << " " << mSalary << endl;
}
ProfessorInfo::~ProfessorInfo() {

}