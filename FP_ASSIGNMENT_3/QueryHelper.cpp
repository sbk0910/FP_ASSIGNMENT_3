#include "QueryHelper.h"

string trim(string str) {
	if (str[0] == ' ') {
		return str.substr(1);
	}
	return str;
}
QueryHelper::QueryHelper() {
	mStudDB = new DBHelper("Student");
	mProfDB = new DBHelper("Professor");
}

void QueryHelper::GetInfoData() {
	mStudDB->GetStudDataFromFile();
	mProfDB->GetProfDataFromFile();
}

void QueryHelper::GetQueryData() {
	ifstream fin(QUERY_FILE_NAME);
	if (fin.fail()) {
		cout << "\n\n\t\tCould not Find " << QUERY_FILE_NAME << endl;
		exit(0);
	}

	string sData;
	stringstream ss;
	int querySize;
	getline(fin, sData);
	ss.str(sData);
	ss >> querySize;
	for (int i = 0; i < querySize; i++) {
		string sData2;
		getline(fin, sData2);
		AnalyzeQueryState(sData2);
	}
	fin.close();
}
void QueryHelper::AnalyzeQueryState(string _query) {
	stringstream ss;
	ss.str(_query);
	string str[5];
	int cnt = 0;
	while (getline(ss, str[cnt], ',')) {
		cnt++;
	}
	if (str[0] == "Search-Exact") {
		SearchExact(trim(str[1]), trim(str[2]), atoi(str[3].c_str()));
	}
	else if (str[0] == "Search-Range") {
		SearchRange(trim(str[1]), atof(str[3].c_str()), atof(str[4].c_str()));
	}
	else if (str[0] == "Join") {
		Join(trim(str[1]), trim(str[2]));
	}
}

void QueryHelper::SearchExact(string _tableName, string _att, int _key) {
	int blockNum, entryNum;
	cout << "\n\t\t\tSearching " << _att << ": " << _key << "...." << endl;
	if (_tableName == "Students") {
		blockNum = mStudDB->mHashMap->HashFunc(_key);
		entryNum = mStudDB->mHashMap->mTable[blockNum]->GetNumEntry();
		StudentInfo* stds = new StudentInfo[entryNum];
		FILE* fp = fopen("Student.db", "rb");
		fpos_t pos = 4096 * blockNum;
		fsetpos(fp, &pos);

		ofstream fout(QUERY_RESULT_FILE_NAME, ios::app);
		fread(stds, sizeof(StudentInfo), entryNum, fp);
		int cnt;
		if (_att == "studentID") {
			for (int i = 0; i < entryNum; i++) {
				if (stds[i].mStudentID == _key) {
					cout << stds[i].mName << ", "
						<< stds[i].mStudentID << ", "
						<< stds[i].mScore << ", "
						<< stds[i].mAdvisorID << ">" << endl;
					cnt++;
				}
			}
			if (!cnt)
				cout << "\n\t\t" << _tableName << ": " << _att << ": " << _key << " was not founded.." << endl;
			else
				cout << "\n\t\t" << _tableName << ": " << _att << ": " << _key << ": " << cnt << " Results founded!!" << endl;

		}
		else {
			return;
		}
		fout.close();

	}
	else if (_tableName == "Professors") {
		blockNum = mProfDB->mHashMap->HashFunc(_key);
		entryNum = mProfDB->mHashMap->mTable[blockNum]->GetNumEntry();
		ProfessorInfo* profs = new ProfessorInfo[entryNum];
		FILE* fp = fopen("Professor.db", "rb");
		fpos_t pos = 4096 * mProfDB->mHashMap->mTable[blockNum]->GetBlockNumber();
		fsetpos(fp, &pos);
		fread(profs, sizeof(ProfessorInfo), entryNum, fp);
		fclose(fp);
		bool found = false;

		int cnt = 0;
		ofstream fout(QUERY_RESULT_FILE_NAME, ios::app);
		if (_att == "ProfID") {
			for (int i = 0; i < entryNum; i++) {
				if (profs[i].mProfID == _key) {
					fout << "<"
						<< profs[i].mName << ", "
						<< profs[i].mProfID << ", "
						<< profs[i].mSalary << ">" << endl;
					cnt++;
				}
			}
		}
		if (!cnt)
			cout << "\n\t\t" << _tableName << ": " << _att << ": " << _key << " was not founded.." << endl;
		else
			cout << "\n\t\t" << _tableName << ": " << _att << ": " << _key << ": " << cnt << "Results founded!!" << endl;

		fout.close();
	}
	else {
		cout << "SomeThing Wrong.." << endl;
		exit(0);
	}


}
void QueryHelper::SearchRange(string _tableName, float _min, float _max) {
	list<int> block_N;
	list<int>::iterator bit;
	int entryNum;
	ofstream fout(QUERY_RESULT_FILE_NAME, ios::app);
	if (_tableName == "Students") {
		block_N = Range_Search("Student_Score", QUERY_RESULT_FILE_NAME, _min, _max, mStudDB->mSroot_num, mStudDB->mSnum_nodes);

		for (bit = block_N.begin(); bit != block_N.end(); bit++) {
			entryNum = mStudDB->mHashMap->mTable[*bit]->GetNumEntry();
			StudentInfo* stds = new StudentInfo[entryNum];
			FILE* fp = fopen("Student.db", "rb");
			fpos_t pos = 4096 * *bit;
			fsetpos(fp, &pos);
			fread(stds, sizeof(StudentInfo), entryNum, fp);
			fclose(fp);
			for (int i = 0; i < entryNum; i++) {
				if (stds[i].mScore >= _min && stds[i].mScore <= _max) {
					/*
					cout << stds[i].mName << ' '
						<< stds[i].mStudentID << ' '
						<< stds[i].mScore << ' '
						<< stds[i].mAdvisorID << endl;
						*/
					fout << stds[i].mName << ' '
						<< stds[i].mStudentID << ' '
						<< stds[i].mScore << ' '
						<< stds[i].mAdvisorID << endl;
				}
			}
			delete[] stds;
		}
	}
	else if (_tableName == "Professors") {
		block_N = Range_Search("Professor_Salary", QUERY_RESULT_FILE_NAME, _min, _max, mProfDB->mProot_num, mProfDB->mPnum_nodes);

		for (bit = block_N.begin(); bit != block_N.end(); bit++) {
			entryNum = mStudDB->mHashMap->mTable[*bit]->GetNumEntry();
			ProfessorInfo* profs = new ProfessorInfo[entryNum];
			FILE* fp = fopen("Professor.db", "rb");
			fpos_t pos = 4096 * *bit;
			fsetpos(fp, &pos);
			fread(profs, sizeof(ProfessorInfo), entryNum, fp);
			fclose(fp);
			for (int i = 0; i < entryNum; i++) {
				if (profs[i].mSalary >= _min && profs[i].mSalary <= _max) {
					/*
					cout << profs[i].mName << ' '
						<< profs[i].mProfID << ' '
						<< profs[i].mSalary << endl;
						*/
					fout << profs[i].mName << ' '
						<< profs[i].mProfID << ' '
						<< profs[i].mSalary << endl;
				}
			}
			delete[] profs;
		}
	}
	fout.close();
}
void QueryHelper::Join(string _tableName1, string _tableName2) {
	int studTableSize = mStudDB->mHashMap->mTableSize;
	int profTableSize = mProfDB->mHashMap->mTableSize;
	cout << "\n\n\t\t\tJoin with " << _tableName1 << " and " << _tableName2 << endl;
	ofstream fout(QUERY_RESULT_FILE_NAME, ios::app);
	int cnt = 0;
	for (int stdTableNum = 0; stdTableNum < studTableSize; stdTableNum++) {
		int stdNumEntry = mStudDB->mHashMap->mTable[stdTableNum]->GetNumEntry();
		StudentInfo* stds = new StudentInfo[stdNumEntry];
		FILE* stdfp = fopen("Student.db", "rb");
		fpos_t stdPos = 4096 * mStudDB->mHashMap->mTable[stdTableNum]->GetBlockNumber();
		fsetpos(stdfp, &stdPos);
		fread(stds, sizeof(StudentInfo), stdNumEntry, stdfp);
		fclose(stdfp);

		for (int profTableNum = 0; profTableNum < profTableSize; profTableNum++) {
			int profNumEntry = mProfDB->mHashMap->mTable[profTableNum]->GetNumEntry();
			ProfessorInfo* profs = new ProfessorInfo[profNumEntry];
			FILE* profp = fopen("Professor.db", "rb");
			fpos_t proPos = 4096 * mProfDB->mHashMap->mTable[profTableNum]->GetBlockNumber();
			fsetpos(profp, &proPos);
			fread(profs, sizeof(ProfessorInfo), profNumEntry, profp);
			fclose(profp);


			for (int i = 0; i < stdNumEntry; i++)
				for (int j = 0; j < profNumEntry; j++) {
					if (stds[i].mStudentID == profs[j].mProfID) {
						/*cout << "\n\n\t\t\tFOUNDED!!!" << endl;
						cout << "\t" << stds[i].mName << ' '
						<< stds[i].mStudentID << ' '
						<< stds[i].mScore << ' '
						<< stds[i].mAdvisorID << endl;
						cout << "\t\t\t" << profs[j].mName << ' '
						<< profs[j].mProfID << ' '
						<< profs[j].mSalary << endl;*/
						cnt++;
						fout << "<" << stds[i].mName << ", "
							<< stds[i].mStudentID << ", "
							<< stds[i].mScore << ", "
							<< stds[i].mAdvisorID << "> , <"
							<< profs[j].mName << ", "
							<< profs[j].mProfID << ", "
							<< profs[j].mSalary << ">" << endl;
					}
				}
		}
	}

	if (!cnt)
		cout << "\n\t\tJoin with" << _tableName1 << "and " << _tableName2 << ": no result was founded.." << endl;
	else
		cout << "\n\t\tJoin with" << _tableName1 << "and " << _tableName2 << ": " << cnt << " results were founded!!" << endl;

	fout.close();
}