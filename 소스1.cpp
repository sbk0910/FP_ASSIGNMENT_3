if (mTable[hash]->IsFull()) {
	HandleOverflow(hash, _key);
	return;
}

if (mTable[hash]->GetFirst() == nullptr) {
	mTable[hash]->SetFirst(new LinkedHashEntry(_key, hash));
}
else {
	LinkedHashEntry *entry = mTable[hash]->GetFirst();
	while (entry->GetNext() != NULL)
		entry = entry->GetNext();
	entry->SetNext(new LinkedHashEntry(_key, hash));
}

void HashMap::Redistribute(int _oldtablenum, Bucket* _bucket1, Bucket* _bucket2) {
	LinkedHashEntry* entry = mTable[_oldtablenum]->GetFirst();
	int newLocalDepth = _bucket1->GetLocalDepth();
	int blockNumOfFirst = entry->GetKey() % (int)pow(2, newLocalDepth);

	while (entry != nullptr) {
		int key = entry->GetKey();
		int blockNum = entry->GetKey() % (int)pow(2, newLocalDepth);
		if ((entry->GetKey() % (int)pow(2, newLocalDepth)) == blockNumOfFirst)
			_bucket1->Append(new LinkedHashEntry(key, blockNum));
		else
			_bucket2->Append(new LinkedHashEntry(key, blockNum));
		entry = entry->mNext;
	}
}